pragma solidity ^0.8.3;

// Token - Tokenomics

// This token is used for governance dividend distribution.  1,000 tokens will be offered pre-launch.
// The pre-launch funds will be used to fund development of a front-end interface that will be available
// open source to the public. An additional 1,000 tokens will be minted for the developers as a long term
// investment in continued development.

// Tokens are minted at a rate of 0.1 per staked governance share.  Staked governance
// shares are moved to the public sales pool for others to purchase the ability to
// vote from the governance contract.  Contract dividends are gained through sales
// of the public sales pool.  Dividends are claimed through burning the token for
// a proportional share of the current network coin held by the token contract.

// Staking, or "cold staking" is accomplished through interaction with the staking method
// in this contract.  Staked governance shares (Shares purchased from the public pool)
// cannot be staked.  Original Governance Shares (OGS) and Public Governance Shares (PGS) will
// be differentiated within the governance contract to allow methods of purchase of both.
// OGS supply will eventually decompose into PGS through normal contract interactions.

// IMPORTANT: PGS cannot be purchased until no more OGS is available to purchase.

// OGS can only be minted via governance vote.  During every governance vote a portion of
// the OGS voted with will be returned to the OGS pool while the remainder will be converted to
// PGS.  Additionally, a governance vote to increase the pool of OGS can be held to mint more
// OGS shares.  The amount of OGS being voted on will be minted if it passes with a
// qualified majority of 70%.

// OGS will be functionally non-fungible, however PGS will be represented through a fungible token.
// Owners of OGS can convert it into staked governance shares or votes only, and cannot transfer
// the shares to another wallet address.  Owners of PGS can only convert it into votes, however it
// will also have the standard functions of an ERC20 token.

// IMPORTANT!
// A PROJECT TRUST CONTRACT WILL ALWAYS HAVE AT LEAST ONE OF THE FOLLOWING:

// - HAVE BOTH THE CONTRACT ADDRESS AND THE SOURCE PUBLISHED THROUGH THE OFFICIAL CHANNEL
//      (https://bitbucket.org/project-trust/)
// - HAVE THE CONTRACT ADDRESS LISTED IN THE OFFICIAL GATEKEEPER CONTRACT

// DO NOT TRUST ANY CONTRACT CLAIMING TO BE AN OFFICIAL PROJECT TRUST CONTRACT THAT DOES NOT INCLUDE AT LEAST
// ONE OF THESE.



import "safe-math.sol";
import "./IERC20.sol";
import "./extensions/IERC20Metadata.sol";
import "./utils/Context.sol";
import "./governor.sol";

contract Trust is SafeMath, Context, IERC20, IERC20Metadata {
    address private _governor;
    address private _devs;
    uint public preLaunchBalance;
    uint private stakePool;
    mapping (address => uint256) private _balances;
    mapping (address => mapping (address => uint256)) private _allowances;
    uint256 private _totalSupply;
    string private _name;
    string private _symbol;

    constructor (address memory devs) {
        _name = "Project Trust Coin";
        _symbol = "TRU$T";
        // Start with owner as gov to allow contract launch before governor launch.
        _governor = _msgSender;
        _devs = devs;
        // Developer initial shares
        _mint(_msgSender, 1000 * 10**uint(decimals));
        // Pre-Launch shares
        preLaunchBalance = 1000 * 10**uint(decimals);
        _mint(address(this), preLaunchBalance);
    }

    function () public payable {
        // Verify there is pre-launch to purchase.
        require(preLaunchBalance > 0);

        // Floor to prevent rounding errors generating more pre-launch than intended.
        // Max loss for this would be 4.99E-16 network coins.  Realistically, this amount
        // should not be of significant monetary value.
        uint memory remainder = msg.value % 500;
        uint memory correctedValue = Div(Sub(msg.value, remainder), 500);
        uint memory refundBalance = 0;
        // Check if purchased all of remaining pre-launch, refund overages (after flooring).
        if(correctedValue >= preLaunchBalance) {
            transfer(address(this), _msgSender, preLaunchBalance);
            refundBalance = Multi(Sub(correctedValue, preLaunchBalance), 500) ;
            preLaunchBalance = 0;
            if(refundBalance > 0) {
                _msgSender.transfer(refundBalance);
            }
        } else {
            preLaunchBalance -= correctedValue;
            transfer(address(this), _msgSender, correctedValue);
        }

        // Pay devs unless payment came from dev address then send to stake pool.
        if(_msgSender != _devs) {
            _devs.transfer(Sub(Multi(correctedValue, 500), refundBalance));
        } else {
            stakePool += Sub(Multi(correctedValue, 500), refundBalance);
        }

    }

    function stake(uint amount, address account) public {
        require(_msgSender == _governor);
        _mint(account, Div(amount, 10));
    }

    function releaseOGS(Governor gov, uint24 amount) internal {
        gov.releaseOGS(amount);
    }

    function updateGov(address newGov) public {
        require(_msgSender == _governor);
        _governor = newGov;
    }

    function withdraw(uint amount) public {
        // First check to make sure governance contract is deployed.
        require(stakePool > 0);
        uint memory senderShare = Div(Multi(amount,stakePool),_totalSupply);
        _burn(_msgSender, amount);
        _msgSender.transfer(senderShare);
        releaseOGS(_governor, multi(amount, 10));
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view virtual override returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5,05` (`505 / 10 ** 2`).
     */
    function decimals() public view virtual override returns (uint8) {
        return 18;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view virtual override returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20}.
     *
     * Requirements:
     *
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for ``sender``'s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);

        uint256 currentAllowance = _allowances[sender][_msgSender()];
        require(currentAllowance >= amount, "ERC20: transfer amount exceeds allowance");
        _approve(sender, _msgSender(), currentAllowance - amount);

        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender] + addedValue);
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        uint256 currentAllowance = _allowances[_msgSender()][spender];
        require(currentAllowance >= subtractedValue, "ERC20: decreased allowance below zero");
        _approve(_msgSender(), spender, currentAllowance - subtractedValue);

        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(address sender, address recipient, uint256 amount) internal virtual {
        require(sender != address(0), "ERC20: transfer from the zero address");
        require(recipient != address(0), "ERC20: transfer to the zero address");

        _beforeTokenTransfer(sender, recipient, amount);

        uint256 senderBalance = _balances[sender];
        require(senderBalance >= amount, "ERC20: transfer amount exceeds balance");
        _balances[sender] = senderBalance - amount;
        _balances[recipient] += amount;

        emit Transfer(sender, recipient, amount);
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), "ERC20: mint to the zero address");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply += amount;
        _balances[account] += amount;
        emit Transfer(address(0), account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), "ERC20: burn from the zero address");

        _beforeTokenTransfer(account, address(0), amount);

        uint256 accountBalance = _balances[account];
        require(accountBalance >= amount, "ERC20: burn amount exceeds balance");
        _balances[account] = accountBalance - amount;
        _totalSupply -= amount;

        emit Transfer(account, address(0), amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
     *
     * This internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(address owner, address spender, uint256 amount) internal virtual {
        require(owner != address(0), "ERC20: approve from the zero address");
        require(spender != address(0), "ERC20: approve to the zero address");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Hook that is called before any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of ``from``'s tokens
     * will be to transferred to `to`.
     * - when `from` is zero, `amount` tokens will be minted for `to`.
     * - when `to` is zero, `amount` of ``from``'s tokens will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual {
        // No 0 value transactions.
        require(amount > 0);
    }
}