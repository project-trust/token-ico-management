pragma solidity ^0.8.3;

// Maths.

contract SafeMath {
    function Add(uint a, uint b) public pure returns (uint c) {
        require (a <= (a + b));
        c = a + b;
    }

    function Sub(uint a, uint b) public pure returns (uint c) {
        require (a >= (a - b));
        c = a - b;
    }

    function Multi(uint a, uint b) public pure returns (uint c) {
        require (b == 0 || (a * b) / b == a);
        c = a * b;
    }

    function Div(uint a, uint b) public pure returns (uint c) {
        require (b > 0);
        c = a / b;
    }
}