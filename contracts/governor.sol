pragma solidity ^0.8.3;

// Contract to manage the initial launch of a coin offering and perform administrative tasks.

import "./safe-math.sol";
import "./trust-coin.sol";
import "./pgs.sol";

contract Governor is SafeMath {
    address public GateKeeper;
    address private owner;
    bool public PrivateOwner;
    PGS public PGSToken;
    Trust public TrustToken;
    uint[] internal ActiveVotes;
    Resolution[] public Votes;
    mapping (address => uint24) private _OGSBalances;

    uint8 constant ChangeGatekeeper = 0;
    uint8 constant ChangeShareCost = 1;
    uint8 constant ChangeVIC = 2; //Vote Initiation Cost
    uint8 constant MintOGS = 3;
    uint8 constant MintPGS = 4;
    uint8 constant ChangeGovernor = 5;

    uint public TotalShares = 100000; // Total OGS supply;
    uint public ShareCost; // How much network coin it costs to purchase a voting share.
    uint public VoteInitiationCost;  // How many vote shares need to be burned to start a vote;

    event StakePurchased(address indexed buyer, uint256 value);
    event VoteSuccess(address indexed voter, bool success);

    struct Resolution {
        uint8 VoteType;   // what is being done?
        string Description; // Why?
        address InitiatedBy; // Who called the vote?
        uint AffirmativeCount; // number of yes votes
        uint NegativeCount; // number of no votes
        bool Active; // Can people still vote?
        uint Deadline; // Unix timestamp end of vote
        uint VoteIntData; // Integer variables relevant to vote;
        address VoteAddressData; // Address variable for address related votes.
    }

    constructor() {
        owner = msg.sender;
        _OGSBalances[owner] = TotalShares;
        PrivateOwner = true;
        StakeCost = 100;
        VoteInitiationCost = 10;
        PGSToken = new PGS("Project Trust PGS", "PGS");
        TrustToken = new Trust();
        maxVote = 0;
    }

    function checkUpdates() internal view returns(bool) {
        return (block.timestamp >= Votes[ActiveVotes[0]].Deadline);
    }

    function doUpdates() internal {
        if(block.timestamp >= Votes[ActiveVotes[0]].Deadline) {
            Votes[ActiveVotes[0]].Active = false;
            votePassOrFail(Votes[ActiveVotes[0]]);
            if(ActiveVotes.length > 1) {
                for(uint a = 0; a < ActiveVotes.length - 1;a++) {
                    ActiveVotes[a] = ActiveVotes[a + 1];
                }
            }
            ActiveVotes.pop();
        }
        if(checkUpdates) doUpdates();
    }

    function votePassOrFail(Resolution thisVote) internal {
        uint8 vType = thisVote.VoteType;

        if(vType == ChangeGatekeeper) {
            if(thisVote.AffirmativeCount >= 1) {

            }
        } else if (vType == ChangeGovernor) {

        } else if (vType == ChangeShareCost) {

        } else if (vType == ChangeVIC) {

        } else if (vType == MintOGS) {

        } else if (vType == MintPGS) {

        } else {
            revert("This shouldn't have happened");
        }
    }

    function _ExecuteGatekeeperChange(address newGatekeeper) {

    }

    function _ExecuteShareCostChange(uint newShareCost) {

    }

    function _ExecuteVICChange (uint newVIC) {

    }

    function _ExecuteMintOGS (uint mintAmount) {

    }

    function _ExecuteMintPGS (uint mintAmount) {

    }

    function _ExecuteChangeGovernor (address newGovernor) {

    }


    function _burnForVote(address burnFrom) internal {
        if(checkUpdates) doUpdates();
        PGSToken.burnForVote(burnFrom, VoteInitiationCost);
    }

    function Stake(uint amount) public {
        // Can only stake OGS
        if(checkUpdates) doUpdates();
        require(_OGSBalances[msg.sender] >= amount);
        _OGSBalances[msg.sender] -= amount;
        TrustToken.stake(amount, msg.sender);
    }

    function BuyOGS() public payable {
        // Allows purchase of OGS if available.
        if(checkUpdates) doUpdates();
    }

    function BuyPGS() public payable {
        // Allows purchase of PGS if available.
        if(checkUpdates) doUpdates();
    }

    function BuyVS(bool InvestOGS) public payable {
        // Purchases voting shares. If InvestOGS flag is true it will automatically stake
        // any OGS purchased this way.  This should be the default method for purchasing voting
        // shares.  OGS will be purchased before PGS.
        if(checkUpdates) doUpdates();
    }

    function Vote(uint ResolutionID, uint Amount, bool PassVote) public {
        // Allows users to vote on resolutions.  Will burn any PGS used for this and return any OGS
        // used this way to the OGS pool.  Burns PGS before OGS.  Converts OGS into PGS decimal size for vote.
        if(checkUpdates) doUpdates();
        require(Votes[VoteKey].Active && block.timestamp < Deadline);
        _burnToVote(msg.sender, Amount);
        if(PassVote) {
            Votes[VoteKey].AffirmativeCount = Add(Votes[VoteKey].AffirmativeCount, Amount);
        } else {
            Votes[VoteKey].NegativeCount = Add(Votes[VoteKey].NegativeCount, Amount);
        }

        emit VoteSuccess(msg.sender, true);
    }

    function getVotableBalance(address voterAddress) public returns (uint) {
        if(checkUpdates) doUpdates();
        return Add((_OGSBalances[msg.sender] * 10**PGSToken.decimals), PGSToken.balanceOf(voterAddress));
    }

    function _burnToVote(address voterAddress, uint Amount) internal {
        if(checkUpdates) doUpdates();
        uint tBal = getVotableBalance(voterAddress);
        require (tBal >= Amount);
        uint pBal = PGSToken.balanceOf(voterAddress);

        if(pBal >= Amount) {
            PGSToken.burnForVote(voterAddress, Amount);
        } else {
            uint Remainder = Div((tBal - pBal), 10**PGSToken.decimals);
            require(_OGSBalances[msg.sender] >= Remainder && Remainder > 0);
            _OGSBalances[msg.sender] -= Remainder;
        }
    }

    function getVote(address VoteKey) public view {
        // Get information on a specific governance vote.

    }

    function getOwnerStake() public view {
        //  Full transparency.  We want the world to know how much the contract owner still holds in governance shares.
        return _VoteStake[owner];
    }

    function setGatekeeperByOwner(address KeeperAddress) public {
        //  Allow owner to update gatekeeper contract if they still hold at least 50% of shares.
        require(msg.sender == owner && _VoteStake[owner] >= Div(TotalStakes, 2));
        GateKeeper = KeeperAddress;
    }

    function setGatekeeperByVote(address VoteKey) private {
        //  Allow governance voting for gatekeeper contract, but only if owner has released more than 50% of shares.
        require(_VoteStake[owner] < Div(TotalStakes, 2));
        GateKeeper = KeeperAddress;
    }

    function setStakeCostByOwner(uint Cost) public {
        //  Setting the amount of Exp needed to buy a stake.  Amount passed should be full 18 decimal value.
        //  Stake cost only impacts owner held shares, no other stake can be purchased through this contract.
        //  How profits from staking are used needs to be fully addressed by the owner to preserve transparency

        //  Keep in mind when setting stake cost that there is a whole number of stakes, overpayments for stakes should be refunded.

        require(msg.sender == owner && _VoteStake[owner] >= Div(TotalStakes, 2) && Cost > 0);
        StakeCost = Cost;
    }

    function setStakeCostByVote(uint Cost) private {
        //  Allows voters to modify stake purchase cost once more than 50% of stake has been purchased.
        //  Sets a minimum cap of cost at one millionth of a full chain coin.
        require(Cost > (0.0001 * 10**18) && _VoteStake[owner] < Div(TotalStakes, 2));
        StakeCost = Cost;

    }

}

