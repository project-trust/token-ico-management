pragma solidity ^0.8.3;

// Contract to manage the initial launch of a coin offering and perform administrative tasks.

import "safe-math.sol";

contract GateKeeper is SafeMath {
    address public Governor;
    address public Factory;
    address private owner;
    address[] public BlackList;

    constructor(address gov) {
        owner = msg.sender;
        Governor = gov;
    }

    function isBlacklisted(address Verify) public view returns (bool) {
        for(uint a = 0; a < BlackList.length; a++) {
            if(Verify == BlackList[a]) { return true; }
        }
        return false;
    }


}

