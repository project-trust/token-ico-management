# Project Trust whitepaper #

(This paper is currently a work in progress and current text may be modified and/or extended as the project develops.)

### Abstract ###

When investors buy in to new tokens on a blockchain network they are often accepting a significant amount of risk.  Under normal circumstances this is to be expected as project success is never a guarantee; however, with blockchain protocols there is often a lack of accountability which leads to bad actors that seek to defraud investors.  To mitigate risk the investors must have some assurance that they are able to recoup at least some of the losses.  This project seeks to solve for this situation through the use of smart contracts. 

### How is risk mitigated? ###

**Project Collateral**

The first step to mitigating investor risk is providing remediation for projects that fail to perform to investor's expectations.  This is achieved through the use of collateral which is provided by the owner of the token.  The collateral amount required is based on the asking price and supply of the initial coin offering (ICO).  The minimum amount of collateral required to perform an ICO on the platform is 50% of the total valuation of the ICO in the native coin ($EXP).  The maximum collateral allowed is 100% of the total valuation of the ICO in the native coin.  Project owners must provide the full supply of the ICO to the Project Trust ICO contract 

Investors must make ICO purchases using the native coin and the Project Trust ICO contract for the token.  The funds used for purchases will be immediately released to the project owner wallet address that was provided during creation of the project.  Once the ICO ends any collateral over the amount collected during the offering will be returned to the project owner while the remaining collateral is held by the contract.  If the amount of collateral provided is less than the full valuation of the project it will be held as long as a matching amount of purchases were made.

As certain conditions are met based on the configuration of the contract, which will be visible to investors prior to investment, the collateral is released to the project leaders until the point at which all collateral minus applicable fees has been returned to the project wallet.  This process can be interrupted by investors utilizing Oversight tokens.  Individual investors can also at any point during this time revert their investment for a share of the remaining collateral relative to their overall investment.  In this case there may still be loss.  However, this will help provide incentive for the developers to complete projects while also providing investor confidence.

**Accountability to investors**

When investors make a purchase through a Project Trust ICO contract they are provided with both the token for the ICO and a matching ICO Oversight token.  The oversight tokens have multiple uses.  The primary use is to reclaim capital in a project the investor feels is not performing.  In order to reclaim capital the investor must initiate a request and send the ICO tokens to the Project Trust ICO contract through the contract interface. Oversight tokens belonging to the investor will be burned at a one to one rate with the ICO tokens that were returned and the investor will be refunded a proportional share of the remaining collateral held in the contract.  At the point which the Project Trust ICO contract has remitted all collateral to the project owners, all oversight tokens for that contract will be burned from all accounts and the contract will self destruct.

A secondary use of the oversight tokens is to call for a delisting of the project token.  A delisting is a vote to indicate that the ICO project is defunct and/or fraud was involved.  When this happens all remaining collateral is returned to all investors regardless of whether they returned the ICO tokens, and all of the addresses associated with the project owners will be blacklisted from further access to Project Trust both as a token owner and as an investor.

In order for a delisting vote to be successful there must be a return of 100% of the ICO token to the contract by investors voting for the delisting (based on votes cast), and at least 70% of investor addresses must have voted for the delisting.  This measure is both for the safety of the investors and the project owners.  Additionally, no less than 50% of the tokens from the and adjusted supply of the ICO must be returned by investors voting for the proposal for success.  The adjusted supply is calculated to prevent a single investor from blocking all other investors from passing the vote and to prevent a small number of investors from passing the vote.

